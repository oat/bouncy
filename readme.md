![bouncy](./logo.png)

_cool music effects in your terminal_

bouncy is a tiny script that hooks onto mpris to read your currently listening song and displays it in a nice way. perfect for sets, rice screenshots or just for casual use

![preview image](./preview.png)

## prerequisites

- a machine running dbus and mpris (typically linux)
- nodejs >=14

## how-to install

`npm install` and `node index.js`

the music player string defaults to `org.mpris.MediaPlayer2.rhythmbox`, however you can use the `BOUNCY_MUSICPLAYER` environment value to change it to another music player name

## usage

press space to switch between modes