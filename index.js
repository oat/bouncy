const render = require('./render');
const mpris = require('./mpris');

async function main() {
  process.title = 'bouncy';
  mpris.init(render.error, render.warning, render.info);
  setInterval(() => {render.render(mpris.getArtist(), mpris.getAlbum(), mpris.getTitle(), mpris.getSongStart(), mpris.getSongEnd(), mpris.getSongPauseSpot(), mpris.getSongPaused())}, render.refreshrate);
}

main();